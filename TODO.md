## Exercise
 
Create a configuration based on docker to run the application (described in the [README.md](https://gitlab.com/pierr/uploader-app/blob/master/README.md) ) in production (simple way).


## Requirements


- [ ] All scripts and configurations must be saved in a Git Repository on Github or Gitlab to be reviewed easily.
- [ ] The **application part of the source code** must not be forked or modified, just cloned without modifications. ( But you can fork the repository of this exercise)



## Expected result


- [ ] The application user data (database and uploaded files) must be keeped if the containers are stopped, deleted and restarted.
- [ ] The configuration should be multi-container but a docker host cluster system (like swarm or kubernetes) is not required




## Bonus

- [ ] The app must listen on HTTPS (port 443) (a self-signed SSL certificate is sufficient, no needs for vendor SSL certificate or Let's encrypt)
HTTP (port 80) must redirect to HTTPS (port 443) (_would be really appreciated_)
- [ ] The mongoDB connection must be authenticated (Database username/password)
- [ ] Propose any improvement that demonstrates your skills